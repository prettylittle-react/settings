class Settings {
	get isExperienceEditor() {
		return !!document.documentElement.classList.contains('sitecore-editing');
	}

	get isPreview() {
		return false;
	}

	get isDebug() {
		return this.config.isDebug;
	}

	get config() {
		const config = (SERVER ? {} : window.config) || {};

		const defaults = {
			usePlaceholders: false,
			lazyLoadImages: false,
			isDebug: false,
			googleMapsApiKey: null
		};

		return {...defaults, ...config};
	}
}

export default new Settings();
